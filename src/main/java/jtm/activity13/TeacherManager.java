package jtm.activity13;

import java.sql.*;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

public class TeacherManager {

	protected Connection conn;


	public TeacherManager () {
		// TODO #1 When new TeacherManager is created, create connection to the
		// database server:
		// url =
		// "jdbc:mysql://localhost/?autoReconnect=true&serverTimezone=UTC&characterEncoding=utf8"
		// user = "student"
		// pass = "Student007"
		// Hints:
		// 1. Do not pass database name into url, because some statements
		// for tests need to be executed server-wise, not just database-wise.
		// 2. Set AutoCommit to false and use conn.commit() where necessary in
		// other methods
		try {


			conn = DriverManager.getConnection ( "jdbc:mysql://localhost/?autoReconnect=true&serverTimezone=UTC&characterEncoding=utf8", "root", "" );
			conn.setAutoCommit ( false );

		} catch (Exception e) {

			System.err.println ( e );
		}
	}


	/**
	 * Returns a Teacher instance represented by the specified ID.
	 *
	 * @param id the ID of teacher
	 * @return a Teacher object
	 */
	public Teacher findTeacher ( int id ) {
		Teacher theTeacher = new Teacher ( 0, null, null );
		try {
			conn.setAutoCommit ( false );
			PreparedStatement prp = conn.prepareStatement ( "SELECT*FROM database_activity.teacher where id=?" );
			prp.setInt ( 1, id );
			ResultSet rs = prp.executeQuery ();
			if (rs.next ()) {
				theTeacher = new Teacher ( id, rs.getString ( 2 ), rs.getString ( 3 ) );
			}
			conn.commit();
			return theTeacher;
		} catch (SQLException e) {
			e.printStackTrace ();
		}
		// TODO #2 Write an sql statement that searches teacher by ID.
		// If teacher is not found return Teacher object with zero or null in
		// its fields!
		// Hint: Because default database is not set in connection,
		// use full notation for table "database_activity.Teacher"
		return theTeacher;
	}

	/**
	 * Returns a list of Teacher object that contain the specified first name and
	 * last name. This will return an empty List of no match is found.
	 *
	 * @param firstName the first name of teacher.
	 * @param lastName  the last name of teacher.
	 * @return a list of Teacher object.
	 */
	public List<Teacher> findTeacher ( String firstName, String lastName ) throws SQLException {
		// TODO #3 Write an sql statement that searches teacher by first and
		// last name and returns results as ArrayList<Teacher>.
		// Note that search results of partial match
		// in form ...like '%value%'... should be returned
		// Note, that if nothing is found return empty list!
		List <Teacher> teacherList = new ArrayList<> ();
		Teacher teacher = new Teacher (0, null, null);
		String st = "SELECT*FROM database_activity.teacher WHERE firstName LIKE ? AND lastName LIKE ?";
		PreparedStatement prp = conn.prepareStatement ( st );
		prp.setString(1, "%"+firstName+"%");
		prp.setString(2, "%"+lastName+"%");
		ResultSet rs = prp.executeQuery ();
		while (rs.next()){
	teacher = new Teacher(rs.getInt("id"),rs.getString("firstName"), rs.getString("lastName"));
	teacherList.add(teacher);
		}
		return teacherList;
	}
//preparedStatement.setString(1, "%", +firstName+"%");

	/**
	 * Insert an new teacher (first name and last name) into the repository.
	 *
	 * @param firstName the first name of teacher
	 * @param lastName  the last name of teacher
	 * @return true if success, else false.
	 */

	public boolean insertTeacher ( String firstName, String lastName ) {
		// TODO #4 Write an sql statement that inserts teacher in database.
		try {
			PreparedStatement prp = conn.prepareStatement("INSERT INTO database_activity.teacher (firstname, lastname) VALUES (? , ?)  ");
			prp.setString(1, firstName);
			prp.setString(2, lastName);
			prp.executeUpdate();
			conn.commit();
			return true;
		} catch (SQLException e) {

			return false;
		}
	}



	/**
	 * Insert teacher object into database
	 *
	 * @param teacher
	 * @return true on success, false on error (e.g. non-unique id)
	 */
	public boolean insertTeacher ( Teacher teacher ) {
		// TODO #5 Write an sql statement that inserts teacher in database.
		try {
			PreparedStatement prp = conn.prepareStatement("INSERT INTO database_activity.teacher (id, firstname, lastname) VALUES (?, ? , ?)  ");
			prp.setInt(1, teacher.getId());
			prp.setString(2, teacher.getFirstName());
			prp.setString(3, teacher.getLastName());
			prp.executeUpdate();
			conn.commit();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * Updates an existing Teacher in the repository with the values represented by
	 * the Teacher object.
	 *
	 * @param teacher a Teacher object, which contain information for updating.
	 * @return true if row was updated.
	 */
	public boolean updateTeacher ( Teacher teacher ) throws SQLException {

		// TODO #6 Write an sql statement that updates teacher information.
		try {
			PreparedStatement prp = conn.prepareStatement("UPDATE database_activity.teacher SET firstname = ?, lastname = ? WHERE id = ? ");
			prp.setInt(3, teacher.getId());
			prp.setString(1, teacher.getFirstName());
			prp.setString(2, teacher.getLastName());
			if (prp.executeUpdate ()>0) {
				conn.commit ();
				return true;
			}else
				return false;

		} catch (SQLException e) {
			conn.rollback ();
			e.printStackTrace ();
			return false;
		}
	}

	/**
	 * Delete an existing Teacher in the repository with the values represented by
	 * the ID.
	 *
	 * @param id the ID of teacher.
	 * @return true if row was deleted.
	 */
	public boolean deleteTeacher ( int id ) throws SQLException {
		// TODO #7 Write an sql statement that deletes teacher from database.
		
		try {
			conn.setAutoCommit ( false );
			PreparedStatement prp = conn.prepareStatement ( "DELETE FROM database_activity.teacher WHERE id = ?" );
			prp.setInt (1 ,id);
			if (prp.executeUpdate ()>0) {
				conn.commit ();
				return true;
			}else
				return false;
		}
		catch (SQLException e) {
			conn.rollback ();
			e.printStackTrace ();
			return false;
		}
			}

	public void closeConnecion () {
		// TODO Close connection to the database server and reset conn object to null

		try {
			if (conn != null)
				conn.close ();
			conn = null;
		} catch (SQLException e) {
			e.printStackTrace ();

		}

	}
}
