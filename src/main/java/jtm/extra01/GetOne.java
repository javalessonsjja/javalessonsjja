package jtm.extra01;

public class GetOne {

	public static int iterations ( int number ) {
		// TODO #1: Implement method which processes the passed positive number
		// value until it's
		// reduced to 1.
		// If the number is even then divide it by 2. If it is odd then multiply
		// it by 3 and add 1. Count how many iterations
		// it takes to do this calculation and return that count. For example:
		// passed number is 6. Path to completion would be:
		// 6->3->10->5->16->8->4->2->1. Iteration count=8.
		// HINT: Use while loop.

		int iterationCount = 0;

		while (number != 1) {
			iterationCount++;
			if (number % 2 == 0) {
				number = number / 2;

			} else {
				number = number * 3 + 1;
			}
		}
		return iterationCount;
	}


	public static void main ( String[] args ) {

		System.out.println ( "Iteration count is: " + iterations ( 6 ) );
		System.out.println ( "The biggest count of iterations for numbers" + theMostComplexNo ( 3 ) );
	}

	public static int theMostComplexNo ( int maxNumber ) {

		int max = 0;
		for (int i = 1; i <= maxNumber; i++) {
			iterations ( i );
			max = iterations ( i );
			if (iterations ( (i + 1) ) > max) {
				max = iterations ( (i + 1) );
			}
		}
		return max;
	}
}

