package jtm.activity06;

public class Human implements Humanoid {
    int stomach;
    boolean isAlive;
    int weight;

    public Human () {
        stomach = 0;
        weight = BirthWeight;
        isAlive = true;


    }
    @Override
    public void eat ( Integer food ) {
        if (this.stomach == 0) {
            this.stomach = food;
            this.weight += stomach;

        }
        this.weight=getWeight();
    }

    @Override
    public Integer vomit () {
        int currentEaten=0;
        if (this.stomach!=0)
            currentEaten = this.stomach;
            this.weight -=stomach;
            this.stomach=0;
        return currentEaten;
         }

    @Override
    public String isAlive () {
        if (isAlive)
            return "Alive";
        else
        return "Dead";
    }

    @Override
    public String killHimself () {

        isAlive = false;
        return isAlive();
    }

    @Override
    public int getWeight () {
        return weight;
     }

    @Override
    public String toString() {
       /* return "Human: "+getWeight()+" ["+stomach+ "]";*/
        return getClass().getSimpleName()+": "+(this.stomach+BirthWeight)+" ["+this.stomach+ "]";
    }
}
