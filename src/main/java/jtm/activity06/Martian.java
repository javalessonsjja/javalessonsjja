package jtm.activity06;

public class Martian implements Humanoid, Alien, Cloneable {
    int weight;
    Object stomach;

    public Martian() {

        this.weight =Alien.BirthWeight;
    }

    @Override
    public void eat ( Object item ) {
        if (this.stomach == null) {
            this.stomach = item;
            if (item instanceof Humanoid) {
                Humanoid tmp = (Humanoid) item;
                this.weight += tmp.getWeight ();
                tmp.killHimself ();
            }

            if (item instanceof Integer) {
                Integer tmp = (Integer) item;
                this.weight+= tmp;
            }

        }
    }

    @Override
    public void eat ( Integer food ) {
        eat((Object)food);
        }


    @Override
    public Object vomit () {
        this.weight = Alien.BirthWeight;

        if (stomach instanceof Integer && (Integer) this.stomach != 0) {
            Integer copyInt = (Integer) this.stomach;
            this.stomach = null;
            return copyInt;
        } if (stomach instanceof Integer) {
            return this.weight;
        }
        if (stomach == null)
            return null;

        Object copy = this.stomach;
        this.stomach = null;
        return copy;
    }

    @Override
    public String isAlive () {

        return "I AM IMMORTAL!";
    }

    @Override
    public String killHimself () {

        return isAlive();
    }

    @Override
    public int getWeight () {

        return weight;
    }

    public Object clone() throws CloneNotSupportedException {
        return clone (this);
    }
    private Object clone (Object current){
       if (current instanceof Integer )
            return current;

        if (current instanceof Human) {
            Human tmpHuman = (Human) current;
            Human newHuman = new Human();
            newHuman.weight = tmpHuman.weight;
            newHuman.stomach = tmpHuman.getWeight()-tmpHuman.BirthWeight;
            return newHuman;
        }
        if (current instanceof Martian) {
            Martian tmpMartian = (Martian) current;
            Martian newMartian = new Martian();
            newMartian.weight = tmpMartian.getWeight();
            newMartian.stomach = clone (tmpMartian.stomach);
            return newMartian;
        }
        else return null;

    }

    @Override
    public String toString() {
        return getClass().getSimpleName()+": "+weight+" ["+stomach+ "]";
    }

}
