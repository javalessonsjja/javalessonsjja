package jtm.activity05;

import jtm.activity04.Road;
import jtm.activity04.Transport;

public class Amphibia extends Transport {
    private byte sails;
    private int wheels;

    public Amphibia ( String id, float consumption, int tankSize, byte sails, int wheels ) {
    super ( id, consumption, tankSize );
    this.sails=sails;
    this.wheels=wheels;
    }

    @Override
    public String move ( Road road ) {
        if (road instanceof WaterRoad)
            return  super.getType() + " is sailing on " + road + " with " + sails + " sails";
        else
            return super.move(road).replace("moving", "driving") + " with " + wheels +" wheels";
    }
   /* public static void main (String[] args) {
        Amphibia testAmp = new Amphibia ("1", 2.5f, 20, (byte)4, 5);
        System.out.println (testAmp);
    }*/
}
