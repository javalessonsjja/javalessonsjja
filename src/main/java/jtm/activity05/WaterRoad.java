package jtm.activity05;

public class WaterRoad extends jtm.activity04.Road {
    public WaterRoad ( String from, String to, int distance ) {
        super ( from, to, distance );
    }
    public WaterRoad() {

    }

    @Override
    public String toString () {
        return this.getClass ().getSimpleName () + " " + this.getFrom () + " — "
                + this.getTo () + ", " + this.getDistance () + "km";
    }
}
