package jtm.activity03;

public class RandomPerson {

    // TODO Create _private_ structure of random person to store values:
    // name as String,
    // age as int,
    // weight as float,
    // isFemale as boolean;
    // smile as char
    // HINT: use Alt+Shift+A to switch to Block selection (multi-line cursor)
    // to edit list of all properties at once
    public String name;
    public int age;
    public float weight;
    public boolean isFemale;
    public char smile;

    public RandomPerson ( String name, int age, float weight, boolean isFemale, char smile ) {
        this.name = name;
        this.age = age;
        this.weight = weight;
        this.isFemale = isFemale;
        this.smile = smile;
    }

    // TODO Select menu "Source — Generate Getters and Setters..." then select
    // all properties and generate _public_ getters and setters for all of them

    public String getName () {
        return this.name;
    }

    public void setName ( String name ) {
        this.name = name;
    }

    public int getAge () {
        return this.age;
    }

    public void setAge ( int age ) {
        this.age = age;
    }

    public float getWeight () {
        return this.weight;
    }

    public void setWeight ( float weight ) {
        this.weight = weight;
    }

    public boolean isFemale () {
        return this.isFemale;
    }

    public void setFemale ( boolean female ) {
        isFemale = female;
    }

    public char getSmile () {
        return this.smile;
    }

    public void setSmile ( char smile ) {
        this.smile = smile;
    }

    public String toString () {
        return "Name: " + name + ", age: " + age + ", weight: " + weight + " kg, " + " Is female?: "+isFemale + ", "+ smile;
    }

    public static void main ( String[] args ) {
        RandomPerson person = new RandomPerson ( "Jānis", 36, 90, false, '\u263A' );
        System.out.println ( person.toString () );
    }
}
